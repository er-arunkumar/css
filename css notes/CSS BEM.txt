BEM CSS Methodology:

What is BEM in CSS?

B-Block
E-Element
M-Modifier

BEM is methodology to develop structural web designs!
CSS Naming convention used for class names
Invented by Yandex, Russia based company in 2009
BEM based on Object Oriented CSS (OOCSS) priciple
Similar to object-oriented programming, OOCSS focuses on flexible and reusable components, each doing one thing well. 
Widely used by the frontend community.

What is the problem with CSS?
When project grows, it's little harder to maintain
If multiple team work on same project, there might be conflict.
And lot more things, which you will understand after learning BEM.

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>BEM CSS</title>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100..900;1,100..900&display=swap');

        * {
            margin: 0;
            padding: 0;
            font-family: "Montserrat", sans-serif;
        }

        body {
            box-sizing: border-box;
            display: grid;
            width: 100%;
            min-height: 100vh;
        }

        .menu-card {
            width: 450px;
            height: auto;
            padding: 20px;
            display: grid;
            grid-template-columns: 120px 1fr;
            place-self: center;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            border-radius: 15px;
        }

        .menu-card__img {
            width: 100px;
            height: 105px;
            border-radius: 10px;
        }

        .menu-card__title {
            margin-bottom: 10px;
        }

        .menu-card__desc {
            margin-bottom: 10px;
            font-size: 12px;
        }

        .menu-card__btn {
            display: flex;
        }

        .menu-card__btn button {
            margin-right: 10px;
            flex-grow: 1;
            font-weight: 600;
        }

        .menu-card__btn--primary {
            background-color: #007bff;
            color: #fff;
            border: none;
            padding: 8px 16px;
            border-radius: 5px;
            cursor: pointer;
        }

        .menu-card__btn--secondary {
            background-color: #22c55e;
            color: #fff;
            border: none;
            padding: 8px 16px;
            border-radius: 5px;
            cursor: pointer;
        }
    </style>
</head>

<body>

    <div class="menu-card">
        <img src="https://www.foodiesfeed.com/wp-content/uploads/2023/06/pouring-honey-on-pancakes.jpg" alt="food-image"
            class="menu-card__img">
        <div>
            <h3 class="menu-card__title">
                Honey Pancake
            </h3>
            <p class="menu-card__desc">
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Quasi aliquam.
            </p>
            <div class="menu-card__btn">
                <button class="menu-card__btn--primary">Add to Cart</button>
                <button class="menu-card__btn--secondary">Pay Now</button>
            </div>
        </div>
    </div>


</body>

</html>