CSS

CSS - Casecading Style Sheets
It is a styling language
Used for website layout and design


3 ways for adding css

	- Inline CSS
	- Internal CSS
	- External CSS
	
Inline CSS - Directly write in the HTML element

eg: <h1 style="color: red;">Welcome to CSS</h1>

Internal CSS - using <style></style> tags within a single document

CSS Rule:

	selector{
	propertyName : value;
	}

eg: 
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Simple CSS Example</title>

  <style>

    h1 {
      color: #0066cc;
    }

    p {
      line-height: 1.5;
	  color: blue;
    }
  </style>
  
</head>
<body>

  <h1>Welcome to CSS</h1>
  <p>This is a simple example of internal CSS.</p>

</body>
</html>


External CSS - Linking an external css file. file extension should be ".css" format

eg: index.html

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Simple CSS Example</title>
  <link rel="stylesheet" href="./assets/css/style.css">
</head>
<body>

  <h1>Welcome to CSS</h1>
  <p>This is a simple example of internal CSS.</p>

</body>
</html>


style.css

h1 {
      color: #0066cc;
    }

    p {
      line-height: 1.5;
	  color: blue;
    }
	
CSS Selectors:

Universal Selector (*): Selects all elements on the page.

* {
   color: blue;
}

Element Selector: Selects all instances of a specific HTML element

p {
   font-size: 16px;
}

Class Selector (.classname): Selects elements with a specific class attribute.

.highlight {
   background-color: yellow;
}

ID Selector (#id): Selects a specific element with a unique ID attribute.

#header {
   font-weight: bold;
}

Pseudo-element Selector (::pseudo-element): Selects a specific part of an element.

p::first-line {
   font-weight: bold;
}

Pseudo-class Selector (:pseudo-class): Selects elements based on their state or position.

a:hover {
   color: red;
}

Types of pseudo-class:
5 class
:hover
:focus
:active
:link - anchor tag
:visited - anchor tag

CSS Color:

2 types of colors:

	- background color
	- foreground color
	
color usage:

colorname : red (human readable)
hexadecimal: #ff0000
RGB: rgb(256, 0, 0) 

Typography:

Typefaces:
serif
sans serif
monospace
handwriting / cursive
fantasy

variation

weight - regular, light, semibold, bold
style - normal, italic, oblique
stretch - Extended and condensed

font-family
font-size
font-weight
font (shortand propery)

font-size denoted 4 types.
px
%
em = 100%, 1em {Manual Printing Industry}
rem = 16px, 1rem (size calculated by root element)

Text Transform:

text-transform
lowercase
uppercase
capitilize

Text Decoration:

none
underline
overline
line-through (strick out)

line-height: (leading) no units - fontsize x value

text-align: left, right, center, justify

letter-spacing (tracking) - px,rem
word-spacing
 
css background:
background-color:
background-image:url();
background-repeat:repeat, repeat-x, repeat-y, no-repeat...
background-size: cover, contain
background-attachment:
background-position:right, left, center

Box Properties Intro: 

In HTML 2 types of elements

Inline Elements - Inline elements do not start on a new line and only take up as much width as necessary. 

Ex: <span>, <a> (anchor), and <strong> (bold).

Block Level Elements - Block-level elements start on a new line and occupy the full width available. 

Ex: <div>, <p> (paragraph), and <h1> to <h6> (headings).

Box-Model:

width:

width - normal width
min-width - minimum width of content
max-width - maximum width of content

height

height - normal height
min-height - minimum height of content
max-height - maximum height of content

margin:

The margin property is used to create space around an element. It defines the outer space between the border of an element and the adjacent elements. 

<div class="box">
   This box has a margin of 20px.
</div>

 .box {
        width: 200px;
        height: 100px;
        background-color: #3498db;
        color: #fff;
        margin: 20px; 
      }
border:

The border property is used to define the border of an element. It consists of three parts: width, style, and color.

style type:
dotted
dashed
solid

padding:

The padding property is used to define the space between the content of an element and its border. It adds internal space within the element. 

CSS Links:

Link States: (Pseudo-class)

:link - normal link, user unvisited state
:visited - normal link, user visited state
:hover - a link when the user mouses over it chnage state
:active - normal link, unvisted link
:focus - normal input, selected input box

List:

list-style-type: square, disc, circle, none, decimal, decimal-leading-zero, lower-alpha/roman, upper-alpha/lower

list-style-position: inside or outside

list-style-image:  url (file path / image url)

css table:

border
border-space
border-collapse: separate / collapse
empty-cell: show/hide
padding
margin
:nth-child(odd/even)

Display:block, inline, inline-block, none

Display:flex, grid

Visibility: visible/hidden

box-shadow

4 sides (top/bottom, left/right), inset (inner side shadow)
spread
blur
color

Positioning (Important Topic)

Positioning Types:

Static (Default) -  Elements with this position behave according to the normal flow of the document.
Fixed -  Elements with a fixed position are positioned relative to the browser window. They won't move even if you scroll. 
Ex: footer
Sticky - Sticky positioning is a hybrid of relative and fixed positioning. The element is treated as relative positioned until it crosses a specified point during scrolling, then it becomes fixed
Ex: navbar

Relative - Elements with relative positioning are moved relative to their normal position.
Absolute - Elements with absolute positioning are positioned relative to their closest positioned ancestor, if any. If not, they're positioned relative to the initial containing block.
Ex: popup close button

Transform: none, rotate, skew, scale, translate

float: this property is used to make an element move to the left or right of its container, allowing other elements to wrap around it. It's commonly used for creating layouts where elements are positioned side by side.

float: left/right

clear the float property using clear

clear: left, right, both

Box Sizing: content-box, border-box

The box-sizing property in CSS is used to control how the total width and height of an element is calculated, including its padding and border.

content-box (default): With this value, the width and height you specify in CSS only apply to the content area of the element. Any padding or border added to the element will increase its total width and height.

border-box: With this value, the width and height you specify in CSS include both the content area and any padding or border. This means that the padding and border will be included in the specified width and height, and the content area will shrink accordingly to accommodate them.




















