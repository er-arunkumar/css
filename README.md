# CSS

This repository helps in learning the basics of CSS with hands-on tasks. Important concept like flexbox, grid and media queries are included.

#### Task-#5

![Task#5](./Task%20Images/Task-5.jpg)

#### Task-#6

![Task#6](./Task%20Images/Task-6.jpg)

#### Task-#7

![Task#7](./Task%20Images/Task-7.jpg)

#### Task-#8

![Task#8](./Task%20Images/Task-8.jpg)

#### Task-#9

![Task#9](./Task%20Images/Task-9.jpg)

#### Task-#10

![Task#10](./Task%20Images/Task-10.jpg)

#### Task-#11

![Task#11](./Task%20Images/Task-11.png)

#### Task-#12

![Task#12](./Task%20Images/Task-12.png)

#### Task-#13

![Task#13](./Task%20Images/Task-13.png)

